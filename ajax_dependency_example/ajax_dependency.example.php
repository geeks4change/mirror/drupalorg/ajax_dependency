<?php

use Drupal\ajax_dependency\AjaxDependency;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 *   Ajax dependency example.
 */

/**
 * An example ajax dependency form via form array.
 */
function ajax_dependency_example_form($form, FormStateInterface $formState) {

}

/**
 * An example ajax dependency form via helper methods.
 */
function ajax_dependency_example_form_via_helpers($form, FormStateInterface $formState) {

  $form = [];

  return $form;
}
