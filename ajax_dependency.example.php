<?php
/**
 * @file
 *   Ajax Dependency examples..
 */

use Drupal\ajax_dependency\AjaxDependency;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form.
 *
 * This is an example how to use ajax_dependency in form_alter of a node form.
 *
 * @throws \Drupal\Core\TypedData\Exception\MissingDataException
 */
function h4c_organisation_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
  $formObject = $form_state->getFormObject();
  $node = $formObject->getEntity();
  if ($node->bundle() === 'organisation') {
    $requiredFields = [
      'field_h4c_organisation_types',
      'field_topics',
      'field_h4c_business_types',
      'field_h4c_sustainability',
    ];
    $missingFields = array_diff($requiredFields, array_keys($form));
    if ($missingFields) {
      \Drupal::messenger()->addError(t('Missing fields: %fields', ['%fields' => implode(', ', $missingFields)]));
    }
    else {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $updatedNode */
      $updatedNode = $formObject->buildEntity($form, $form_state);
      $orgTypes = [];
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      foreach ($updatedNode->get('field_h4c_organisation_types') as $item) {
        if ($value = $item->get('value')->getValue()) {
          $orgTypes[$value] = $value;
        }
      }

      $orgTypesWidget =& $form['field_h4c_organisation_types']['widget'];
      AjaxDependency::contentIf($orgTypes['initiative'] ?? NULL, $orgTypesWidget, $form['field_topics'], $form_state);
      AjaxDependency::contentIf($orgTypes['business'] ?? NULL, $orgTypesWidget, $form['field_h4c_business_types'], $form_state);
      AjaxDependency::contentIf($orgTypes['business'] ?? NULL, $orgTypesWidget, $form['field_h4c_sustainability'], $form_state);
    }
  }
}