<?php

namespace Drupal\ajax_dependency;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;

class AjaxDependencyWorker {

  /**
   * Set dependency.
   *
   * We don't know the array parents of the elements yet so we simply mark them.
   *
   * @param array $sourceElement
   *   The source element.
   * @param array $targetElement
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public static function setDependency(&$sourceElement, &$targetElement, FormStateInterface $formState) {
    $sourceElement['#ajax_dependency_target_refs'][] =& $targetElement;
    $sourceElement['#ajax']['callback'] = static::class . '::updateForm';

    self::addNojsSubmitElement($sourceElement, $formState);
    FormTool::addUnique($sourceElement['#after_build'], static::class . '::afterBuildForSource');
    FormTool::addUnique($targetElement['#after_build'], static::class . '::afterBuildForTarget');
  }

  /**
   * After-build callback for source elements.
   *
   * @param array $sourceElement
   *   The source element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   Form api callbacks need the element returned.
   */
  public static function afterBuildForSource(array $sourceElement, FormStateInterface $formState) {
    self::moveNojsSubmitElement($sourceElement, $formState);
    return $sourceElement;
  }

  /**
   * Add a no-js submit element right after the source element.
   *
   * @param array $sourceElement
   *   The source element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  private static function addNojsSubmitElement(array &$sourceElement, FormStateInterface $formState) {
    $noJsSubmitElement = [
      '#type' => 'submit',
      '#value' => t('Dummy'),
      // Hide this button when JavaScript is enabled.
      '#attributes' => ['class' => ['js-hide'], 'formnovalidate' => 1],
      '#validate' => [static::class . '::nojsValidate'],
      '#submit' => [static::class . '::nojsSubmit'],
      // This button will add some value to form state, so care that it does not
      // interfere with other settings by adding an arbitrary unique root key.
      '#parents' => ['ajax_dependency__' . Crypt::randomBytesBase64(8)]
    ];
    if (!empty($sourceElement['#title'])) {
      $noJsSubmitElement['#value'] = t('Update "@title" choice', [
        '@title' => $sourceElement['#title'],
      ]);
    }

    // Copy #weight and #access from the triggering element to the button, so
    // that the two elements will be displayed together.
    foreach (['#weight', '#access'] as $key) {
      if (isset($sourceElement[$key])) {
        $noJsSubmitElement[$key] = $sourceElement[$key];
      }
    }

    $sourceElement['ajax_dependency_nojs_submit'] = $noJsSubmitElement;
  }

  /**
   * Add a no-js submit element right after the source element.
   *
   * @param array $sourceElement
   *   The source element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  private static function moveNojsSubmitElement(array &$sourceElement, FormStateInterface $formState) {
    $noJsSubmitElement = $sourceElement['ajax_dependency_nojs_submit'];
    unset($sourceElement['ajax_dependency_nojs_submit']);

    $completeForm =& $formState->getCompleteForm();
    $sourceParents = $sourceElement['#array_parents'];
    $sourceContainerParents = $sourceParents;
    $sourceKey = array_pop($sourceContainerParents);
    $sourceContainer =& NestedArray::getValue($completeForm, $sourceContainerParents);
    $noJsSubmitKey = $sourceKey . '_ajax_dependency_nojs_submit';

    if (!isset($noJsSubmitElement['#value'])) {
      $noJsSubmitElement['#value'] = t('Update "@title" choice', [
        '@title' => $sourceKey,
      ]);
    }

    // Fix array_parents, leave parents.
    $noJsSubmitParents = $sourceContainerParents;
    $noJsSubmitParents[] = $noJsSubmitKey;
    $noJsSubmitElement['#array_parents'] = $noJsSubmitParents;

    // And insert it right after the source.
    FormTool::insertAfter($sourceContainer, $sourceKey, [$noJsSubmitKey => $noJsSubmitElement]);
  }

  public static function nojsValidate(array $form, FormStateInterface $formState) {
    $formState->clearErrors();
  }

  /**
   * Non-Javascript fallback for updating the form.
   */
  public static function nojsSubmit($form, FormStateInterface $formState) {
    $formState->setRebuild();
  }

  /**
   * After-build callback for target elements.
   *
   * @param array $targetElement
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   Form api callbacks need the element returned.
   */
  public static function afterBuildForTarget(array $targetElement, FormStateInterface $formState) {
    self::replaceWithMockIfNoAccess($targetElement, $formState);
    self::addSelector($targetElement, $formState);
    return $targetElement;
  }

  /**
   * Replace elements with no access with a mock div containing a selector.
   *
   * Otherwise we have no js target to add an element.
   * Form alter is the last step where we see elements with no access.
   *
   * @param array $targetElement
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  private static function replaceWithMockIfNoAccess(array &$targetElement, FormStateInterface $formState) {
    if (FormTool::checkAccess($targetElement) === FALSE) {
      $targetElement['#access'] = TRUE;
      FormTool::addUnique($targetElement['#pre_render'], static::class . '::replaceTargetElementWithMockInPreRender');
    }
  }

  /**
   * Pre render callback for target element.
   *
   * @param array $targetElement
   *   The target element.
   *
   * @return array
   *   The target element.
   */
  public static function replaceTargetElementWithMockInPreRender(array $targetElement) {
    $selector = $targetElement['#ajax_dependency_selector'];
    // Render grouping does its own voodoo and we have tp preserve at least the
    // #printed property to not have our elements twice.
    // @see \Drupal\Core\Render\Element\RenderElement::processGroup
    // @see \Drupal\Core\Render\Element\RenderElement::preRenderGroup
    $targetElement = [
      '#markup' => '<div data-drupal-ajax-dependency-selector="' . $selector . '">' . '</div>',
    ] + array_intersect_key($targetElement, array_flip(['#group', '#groups', '#group_details', '#processed', '#after_build_done', '#attached', '#printed']));
    return $targetElement;
  }

  /**
   * Add a selector (fake id) attribute to target elements.
   *
   * @param array $targetElement
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  private static function addSelector(array &$targetElement, FormStateInterface $formState) {
    $path = $targetElement['#array_parents'];
    $selector = FormTool::uniqueElementId($path, $formState);
    $targetElement += ['#prefix' => '', '#suffix' => ''];
    $targetElement['#prefix'] = '<div data-drupal-ajax-dependency-selector="' . $selector . '">' . $targetElement['#prefix'];
    $targetElement['#suffix'] .= '</div>';
    $targetElement['#ajax_dependency_selector'] = $selector;
  }

  /**
   * Submit callback: Updates a part of the form via AJAX.
   *
   * @return AjaxResponse
   *   Ajax commands to replace parts of the form.
   */
  public static function updateForm($form, FormStateInterface $formState) {
    $triggeringElement = $formState->getTriggeringElement();
    if (isset($triggeringElement['#ajax_dependency_source_ref'])) {
      $sourceElement = $triggeringElement['#ajax_dependency_source_ref'];
    }
    else {
      $sourceElement = $triggeringElement;
    }

    // Sometimes the triggering element is a subelement of the source element.
    // As like in checkboxes, which expands to multiple checkbox elements.
    while (!isset($sourceElement ['#ajax_dependency_target_refs'])) {
      $parents = $sourceElement ['#array_parents'];
      // @todo Yell if this does not terminate.
      array_pop($parents);
      $sourceElement = NestedArray::getValue($form, $parents);
    }

    $response = new AjaxResponse();
    // We do not reference and detach target element to do our modifications.
    foreach ($sourceElement ['#ajax_dependency_target_refs'] as $targetElement) {
      // @see https://www.drupal.org/project/drupal/issues/3063836
      $targetElement['#attached'] = [];
      // Prevent core from moving this to a group, being empty here.
      // @see \Drupal\Core\Render\Element\RenderElement::preRenderGroup
      unset($targetElement['#group']);
      $targetSelectorValue = $targetElement['#ajax_dependency_selector'];
      $targetSelector = sprintf('[data-drupal-ajax-dependency-selector="%s"]', $targetSelectorValue);
      $response->addCommand(new ReplaceCommand($targetSelector, $targetElement));
    }
    return $response;
  }

}
