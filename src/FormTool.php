<?php

namespace Drupal\ajax_dependency;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;

class FormTool {

  /**
   * @return \Drupal\Core\Controller\ControllerResolverInterface
   */
  private static function getControllerResolver() {
    return \Drupal::service('controller_resolver');
  }

  /**
   * Check element access.
   *
   * @see \Drupal\Core\Render\Renderer::doRender
   *
   * @param $element
   *   The element.
   *
   * @return string
   */
  public static function checkAccess(&$elements) {
    if (!isset($elements['#access']) && isset($elements['#access_callback'])) {
      if (is_string($elements['#access_callback']) && strpos($elements['#access_callback'], '::') === FALSE) {
        $elements['#access_callback'] = self::getControllerResolver()->getControllerFromDefinition($elements['#access_callback']);
      }
      $elements['#access'] = call_user_func($elements['#access_callback'], $elements);
    }

    if (isset($elements['#access'])) {
      if ($elements['#access'] instanceof AccessResultInterface) {
        self::addCacheableDependency($elements, $elements['#access']);
        $elements['#access'] = $elements['#access']->isAllowed();
      }
    }

    return $elements['#access'] ?? NULL;
  }

  /**
   * Add cacheable dependency.
   *
   * @param array $elements
   *   The render array.
   * @param $dependency
   *   The dependency.
   */
  public static function addCacheableDependency(array &$elements, $dependency) {
    $meta_a = CacheableMetadata::createFromRenderArray($elements);
    $meta_b = CacheableMetadata::createFromObject($dependency);
    $meta_a->merge($meta_b)->applyTo($elements);
  }

  /**
   * Insert subarray after some key.
   *
   * @param array $array
   *   The array.
   * @param string $key
   *   The key.
   * @param array $insert
   *   The subarray.
   */
  public static function insertAfter(array &$array, $key, array $insert) {
    $keys = array_keys($array);
    $index = array_search($key, $keys);
    $pos = false === $index ? count($array) : $index + 1;
    $array = array_merge(array_slice($array, 0, $pos), $insert, array_slice($array, $pos));
  }

  /**
   * Create a unique form ID.
   *
   * @see \Drupal\Core\Form\FormBuilder::prepareForm
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return string
   *   The ID.
   */
  public static function uniqueFormId(FormStateInterface $formState) {
    $form =& $formState->getCompleteForm();
    return $form['#form_id'];
  }

  /**
   * Create a unique target element DOM ID.
   *
   * @todo Rely on IDs once we have https://www.drupal.org/project/drupal/issues/1852090
   *   So if someone bumps on duplicate or broken IDs from this, we won't
   *   implement more elaborate voodoo hacks here, but please help fix the core
   *   issue.
   *
   * @param array $path
   *   The target element path.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return string
   *   The ID.
   */
  public static function uniqueElementId($path, FormStateInterface $formState) {
    return static::uniqueFormId($formState) . '-' . implode('-', $path);
  }

  public static function addUnique(&$array, $value) {
    if (!isset($array)) {
      $array = [];
    }
    if (!in_array($value, $array)) {
      $array[] = $value;
    }
  }

}
