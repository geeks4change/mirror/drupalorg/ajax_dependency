<?php

namespace Drupal\ajax_dependency;

use Drupal\Core\Form\FormStateInterface;

class AjaxDependency {

  public static function dependsOn(&$sourceElement, &$targetElement, FormStateInterface $formState) {
    AjaxDependencyWorker::setDependency($sourceElement, $targetElement, $formState);
  }

  public static function accessIf($condition, &$sourceElement, &$targetElement, FormStateInterface $formState) {
    $targetElement['#access'] = (bool)$condition;
    self::dependsOn($sourceElement, $targetElement, $formState);
  }

  public static function contentIf($condition, &$sourceElement, &$targetElement, FormStateInterface $formState) {
    if (!$condition) {
      $targetElement['#value'] = NULL;
    }
    self::accessIf($condition, $sourceElement, $targetElement, $formState);
  }

}
